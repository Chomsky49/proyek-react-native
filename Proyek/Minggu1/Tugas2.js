import React, {Component} from 'react'
import { StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'

const index = () => {
    return (
        <View style = {styles.container}>
            <View style = {styles.header}>
                <Text style = {styles.headertext}>Account</Text>
            </View>
            <View style = {styles.header2}>
                <View style = {styles.profile}>
                    <View style = {styles.foto}></View>
                    <Text style = {styles.profiletext}>Abiyan Bagus Baskoro</Text>
                </View>
            </View>
            <View style = {styles.header3}>
                <View style = {styles.kotak3}>
                    <Icon name="wallet-sharp" color = 'black' size = {26}/>
                    <Text style = {styles.textkotak1}>Saldo</Text>
                    <Text style = {styles.harga3}>Rp. 120.000.000 </Text>
                </View>
            </View>
            <View style = {styles.header4}>
                <View style = {styles.kotak4}>
                    <Icon name="settings-sharp" color = 'black' size={26}  />
                    <Text style = {styles.textkotak1}>Pengaturan</Text>
                </View>
             </View>
              <View style = {styles.header5}>
                <View style = {styles.kotak5}>
                    <Icon name="help-sharp" color = 'black' size={26}  />
                    <Text style = {styles.textkotak1}>Bantuan</Text>
                </View>
             </View>
             <View style = {styles.header6}>
                <View style = {styles.kotak6}>
                    <Icon name="newspaper-outline" color = 'black' size={26}  />
                    <Text style = {styles.textkotak1}>wawd</Text>
                </View>
             </View>
             <View style = {styles.header7}>
                <View style = {styles.kotak7}>
                    <Icon name="exit-outline" color = 'black' size={26}  />
                    <Text style = {styles.textkotak1}>Saldo</Text>
                </View>
             </View> 
        </View>
    )}
             
    
    
    


const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : '#DCDCDC'
    },
    header : {
        width: 400,
        height: 50,
        backgroundColor: 'blue',
        borderColor: 'steelblue',
    },
    headertext : {
        color : 'white',
        fontSize : 20,
        textAlign : 'left',
        marginTop : 7,
        marginLeft : 10
    },

// header 2

    header2 : {
        width: 500,
        height: 80,
        backgroundColor: 'white',
        borderColor: 'steelblue',
    },
    
    profile : {
        flexDirection : 'row',
        borderColor : 'grey',
        marginTop : 2,
    },
    foto : {
        height: 50,
        width: 50,
        backgroundColor : 'red',
        borderRadius : 100,
        marginLeft : 20,
        marginTop : 20,
    },
    profiletext : {
        color : 'black',
        fontSize : 20,
        marginTop : 30,
        marginLeft : 20,
    },
    
  // header 3  

    header3 : {
        marginTop : 2,
        width: 500,
        height: 50,
        backgroundColor: 'white',
        borderColor: 'steelblue',

    },
    kotak3 : {
        marginTop: 10,
        flexDirection : 'row',
        borderBottomColor : 'grey',
        marginBottom : 100,
        marginLeft : 20,

    },
    harga3 : {
        alignSelf : 'center', 
        marginLeft : 150,
        marginBottom : 20,
    },
    textkotak1 : {
        marginLeft : 20,
        marginTop : 4,
        marginBottom : 20,
    },

// header 4

    header4 : {
        marginTop : 6,
        width: 500,
        height: 50,
        backgroundColor: 'white',
        borderColor: 'steelblue',
        // marginBottom : 5,
    },
    kotak4 : {
        marginTop: 10,
        marginLeft : 20,
        flexDirection : 'row',
        borderBottomColor : 'grey'
    },
  
// header 5

header5 : {
    marginTop : 1,
    width: 500,
    height: 50,
    backgroundColor: 'white',
    borderColor: 'steelblue',
},
kotak5 : {
    marginTop: 10,
    marginLeft : 20,
    flexDirection : 'row',
    borderBottomColor : 'grey'
},

// header 6

header6 : {
    marginTop : 1,
    width: 500,
    height: 50,
    backgroundColor: 'white',
    borderColor: 'steelblue',
},
kotak6 : {
    marginTop: 10,
    marginLeft : 20,
    flexDirection : 'row',
    borderBottomColor : 'grey'
},

// header 7

header7 : {
    marginTop : 20,
    width: 500,
    height: 50,
    backgroundColor: 'white',
    borderColor: 'steelblue',
},
kotak7 : {
    marginTop: 10,
    marginLeft : 20,
    flexDirection : 'row',
    borderBottomColor : 'grey'
},

    


})

export default index