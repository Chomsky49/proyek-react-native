import React, { isValidElement } from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'

export default class main extends React.Component{
    render(){
    return (
        <View key = {this.props.keyval} style = {styles.note}>   

            <Text style = {styles.noteText}>{this.props.val.date}</Text>
            <Text style = {styles.noteText}>{this.props.val.note}</Text>

            <TouchableOpacity>
            <Icon name="trash-sharp" color = 'white' size={26}  />
            </TouchableOpacity>

        </View>
    )
}
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
    },
    container : {
        flex : 1,
    },
    textheader : {
        fontSize : 20,
        marginLeft : 5,
    },
    headerlist : {
        flexDirection : 'row',
        marginTop : 15,
    },
    InputTodolist : {
        height : 50,
        width : 280,
        backgroundColor : 'transparent',
        marginLeft : 5,
        borderColor : 'black',
        borderWidth : 2
    },
    addbutton : {
        position : 'absolute',
        backgroundColor : '#87CEFA',
        height : 50,
        width : 50,
        alignItems : 'center',
        justifyContent : 'center',
        marginLeft : 10,
    }
})



