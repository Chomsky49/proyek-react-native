import React, { Component } from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import Note from './main'

export default class todolist extends React.Component{

    constructor(props) {
        super(props)
        this.state = {
            noteArray = [],
            noteText = '',
        }
    }

    render(){

        let notes =  this.state.noteArray.map((val, key) => {
            return <Note key = {key} keyval = {key} val = {val}
                deleteMethod = { () => this.deleteNote(key)} />
        })

    return (
        <View style = {styles.container}>
            <Text style = {styles.textheader}>Masukkan Todolist</Text>
            <View style = {styles.headerlist}>
                <View style = {styles.InputTodolist}>
                    <TextInput style = {styles.inputkata}
                    placeholder = 'Input Here'
                    placeholderTextColor = 'black'
                    underlineColorAndroid = 'transparent'
                    ></TextInput>
                </View>
                <View>
                <TouchableOpacity style = {styles.addbutton}>
                    <Text style = {styles.isibutton}>+</Text>
                </TouchableOpacity>
                </View>
            </View>
        </View>
    )
    }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
    },
    textheader : {
        fontSize : 20,
        marginLeft : 5,
    },
    headerlist : {
        flexDirection : 'row',
        marginTop : 15,
    },
    InputTodolist : {
        height : 50,
        width : 280,
        backgroundColor : 'transparent',
        marginLeft : 5,
        borderColor : 'black',
        borderWidth : 2
    },
    addbutton : {
        position : 'absolute',
        backgroundColor : '#87CEFA',
        height : 50,
        width : 50,
        alignItems : 'center',
        justifyContent : 'center',
        marginLeft : 10,
    },
    isibutton : {
        fontSize : 32
    }
})

